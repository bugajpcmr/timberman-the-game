package TimbermanGame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Panel extends JPanel implements KeyListener {
    private Timberman timberman;
    private Tree tree;
    private Panel.Animation animation;
    private String enemyScore;
    private BufferedReader in;
    private PrintWriter out;
    private JFrame frame = new JFrame("Timberman");


    private Panel(Timberman player, Tree tree) throws IOException {
        String serverAddress = "127.0.0.1";
        Socket socket = new Socket(serverAddress, 9001);
        in = new BufferedReader(new InputStreamReader(
                socket.getInputStream()));
        out = new PrintWriter(socket.getOutputStream(), true);
        this.timberman = player;
        this.tree = tree;
        this.enemyScore = "";
        addKeyListener(this);
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
    }

    public static void main(String[] args) throws IOException {
        Timberman timberman = Timberman.create("TimbermanGame/res/role.png","TimbermanGame/res/att_left.png",
                "TimbermanGame/res/att_right.png","TimbermanGame/res/die.png");
        Tree tree = Tree.create("TimbermanGame/res/base.png", "TimbermanGame/res/branchleft.png",
                "TimbermanGame/res/branchright.png", "TimbermanGame/res/body.jpg", "TimbermanGame/res/bg1.jpg");
        Panel panel = new Panel(timberman, tree);
        panel.frame.setContentPane(panel);
        panel.frame.setDefaultCloseOperation(3);
        panel.frame.setSize(500, 530);
        panel.frame.setVisible(true);
        panel.startAnimation();
    }

    private String getNickname() {
        return JOptionPane.showInputDialog(
                frame,
                "Choose a screen name:",
                "Screen name selection",
                JOptionPane.PLAIN_MESSAGE);
    }

    public void paint(Graphics g) {
        super.paint(g);
        this.tree.draw(g);
        g.setFont(new Font("Verdana", Font.BOLD, 20));
        g.setColor(Color.YELLOW);
        g.drawString(enemyScore, 5, 30);
        g.setFont(new Font("Verdana", Font.BOLD, 30));
        g.drawString(""+timberman.getScore(), 225, 150);
        if(timberman.alive) {
            if (timberman.attack)
                this.timberman.drawChop(g);
            else
                this.timberman.draw(g);
        }else
        {
            this.timberman.drawDead(g);
            g.setColor(Color.YELLOW);
            g.fillRect(50, 15, 400, 265);
            g.setFont(new Font("Verdana", Font.BOLD, 25));
            g.setColor(Color.RED);
            g.drawString("GAME OVER", 165, 100);
            g.setColor(Color.BLACK);
            g.drawString("Press ENTER to RESTART", 70, 50);
            g.setFont(new Font("Verdana", Font.BOLD, 25));
            g.drawString("Final Score: "+timberman.getScore(), 140, 145);
            g.drawString("Highest Score: "+timberman.getHighestScore(), 130, 175);
            g.drawString("Enemy scored:", 140, 225);
            g.setColor(Color.RED);
            g.drawString(enemyScore, 155, 255);
        }
    }

    private void checkCollisionWithBranch(){
        if ((timberman.getPosition() && tree.getPosition(0) == 2)||(!timberman.getPosition() && tree.getPosition(0) == 1)){
            timberman.alive = false;

            if(timberman.getHighestScore()<timberman.getScore()) {
                timberman.setHighestScore();
            }
        }else
            timberman.alive = true;
    }

    private void startAnimation() {
        this.animation = new Panel.Animation();
        Thread t = new Thread(this.animation);
        t.start();
    }



    public void stopAnimation() {
        this.animation.stop_animation = true;
    }


    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        int c = e.getKeyCode();
        if(c == KeyEvent.VK_LEFT){
            if(timberman.alive) {
                timberman.setPosition(false);
                attack();
            }
        }
        if(c == KeyEvent.VK_RIGHT){
            if(timberman.alive) {
                timberman.setPosition(true);
                attack();
            }
        }
        if(c == KeyEvent.VK_ENTER) {
            timberman.alive = true;
            timberman.setScore(false);
            repaint();
        }
    }

    private void attack() {
        timberman.attack = true;
        checkCollisionWithBranch();
        tree.choppedBranch();
        timberman.setScore(true);
        out.println(timberman.getScore());
        repaint();
    }

    @Override
    public void keyReleased(KeyEvent e) {
        int c = e.getKeyCode();
        if((c == KeyEvent.VK_LEFT) || (c == KeyEvent.VK_RIGHT)){
            if(timberman.alive) {
                timberman.attack = false;
                repaint();
            }
        }
    }
    class Animation implements Runnable {
        private boolean stop_animation = false;

        Animation() {
        }

        public void run() {
            while(!this.stop_animation) {
                //Panel.this.update();
                try {
                    showScore();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Panel.this.repaint();
                try {
                    Thread.sleep(30L);
                } catch (InterruptedException var2) {
                    var2.printStackTrace();
                }
            }

        }
    }

    private void showScore() throws IOException {
        String line = in.readLine();
        if (line.startsWith("name")) {
            out.println(getNickname());
        } else if (line.startsWith("score")) {
            enemyScore = (line.substring(6) + "\n");
        }
    }
}
