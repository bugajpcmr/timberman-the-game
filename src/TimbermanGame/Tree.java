package TimbermanGame;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;
import java.util.ArrayList;

public class Tree {
    private BufferedImage img_background;
    private BufferedImage img_base;
    private BufferedImage img_branchright;
    private BufferedImage img_branchleft;
    private BufferedImage img_tree;
    private int x;
    private int y;
    private ArrayList<Branch> branches;
    public Tree(BufferedImage base, BufferedImage branchLeft, BufferedImage branchRight, BufferedImage tree, BufferedImage background){
        branches = new ArrayList<Branch>();
        branches.add(new Branch(0));
        for(int i = 0; i < 5; i++)
            addBranch();
        this.img_base = base;
        this.img_branchleft = branchLeft;
        this.img_branchright = branchRight;
        this.img_tree = tree;
        this.img_background = background;
        this.x = 190;
        this.y = 0;
    }

    private void addBranch() {
        Random rand = new Random();
        int x = rand.nextInt(3);
        if(x == 0) {
            branches.add(new Branch(0));        //brak
        }else if(x == 1){
            branches.add(new Branch(1));        //lewo
        }else{
            branches.add(new Branch(2));        //prawo
        }
    }

    public static Tree create(String path1, String path2, String path3, String path4, String path5) throws IOException {
        BufferedImage img1 = ImageIO.read(ClassLoader.getSystemResourceAsStream(path1));
        BufferedImage img2 = ImageIO.read(ClassLoader.getSystemResourceAsStream(path2));
        BufferedImage img3 = ImageIO.read(ClassLoader.getSystemResourceAsStream(path3));
        BufferedImage img4 = ImageIO.read(ClassLoader.getSystemResourceAsStream(path4));
        BufferedImage img5 = ImageIO.read(ClassLoader.getSystemResourceAsStream(path5));
        return new Tree(img1, img2, img3, img4, img5);
    }

    public void choppedBranch() {
        branches.remove(0);
        addBranch();
    }

    public int getPosition(int x){
        Branch br = this.branches.get(x);
        return br.getPosition();
    }

    public void draw(Graphics g){
        int vy = 315;
        g.drawImage(img_background, 0, 0, 500, 500, null);
        g.drawImage(img_base, x-7, y+450, 128, 26, null);
        g.drawImage(img_tree, x, y-26, 112, 480, null);
        for(int i = 0; i < branches.size(); i++)
        {
            if(this.getPosition(i) == 1){
                g.drawImage(img_branchleft, x-114, vy, 118, 57, null);
                vy = vy - 57;
            }else if(this.getPosition(i) == 2){
                g.drawImage(img_branchright, x+109, vy, 118, 57, null);
                vy = vy - 57;
            }else if(this.getPosition(i) == 0){
                vy = vy - 57;
            }
        }
    }


}
