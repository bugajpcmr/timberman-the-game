package TimbermanGame;

public class Branch {
    private int position;           //0 - null, 1 - left, 2 - right
    public Branch(int position)
    {
        this.position = position;
    }
    public int getPosition(){
        return position;
    }
}
