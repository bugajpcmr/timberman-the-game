package TimbermanGame;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.IOException;

public class Timberman {
    private Image img_idle;
    private Image img_att_left;
    private Image img_att_right;
    private Image img_dead;
    private boolean position;           //true - prawo, false - lewo
    private int score;
    private int highestScore;
    private int x;
    private int y;
    public boolean alive;
    public boolean attack;

    public Timberman(BufferedImage idle, BufferedImage left, BufferedImage right, BufferedImage dead) {
        this.img_idle = idle;
        this.img_att_left = left;
        this.img_att_right = right;
        this.img_dead = dead;
        this.x = 98;
        this.y = 480;
        this.score = 0;
        this.highestScore = 0;
        this.position = true;           //true - prawo, false - lewo
        this.alive = true;
        this.attack = false;
    }

    public void draw(Graphics g) {
        if(getPosition()){
            g.drawImage(this.img_idle, this.x+205, this.y-140, 98, 140, (ImageObserver)null);
        }
        else{
            g.drawImage(this.img_idle, this.x-10, this.y-140, 98, 140, (ImageObserver)null);
        }
    }
    public void drawChop(Graphics g) {
        if(getPosition()){
            g.drawImage(this.img_att_right, this.x+145, this.y-120, 152, 128, (ImageObserver)null);
        }
        else{
            g.drawImage(this.img_att_left, this.x, this.y-120, 152, 128, (ImageObserver)null);
        }
    }
    public void drawDead(Graphics g){
        g.drawImage(this.img_dead, this.x+155, this.y-120, 152, 128, (ImageObserver)null);
    }


    public static Timberman create(String path1, String path2, String path3, String path4) throws IOException {
        BufferedImage img1 = ImageIO.read(ClassLoader.getSystemResourceAsStream(path1));
        BufferedImage img2 = ImageIO.read(ClassLoader.getSystemResourceAsStream(path2));
        BufferedImage img3 = ImageIO.read(ClassLoader.getSystemResourceAsStream(path3));
        BufferedImage img4 = ImageIO.read(ClassLoader.getSystemResourceAsStream(path4));
        return new Timberman(img1, img2, img3, img4);
    }

    public boolean getPosition() {
        return position;
    }

    public void setPosition(boolean x) {
        this.position = x;
    }

    public int getScore(){
        return score;
    }
    public void setHighestScore(){
        this.highestScore = this.getScore()+1;
    }
    public int getHighestScore(){
        return highestScore;
    }

    public void setScore(boolean x){
        if(x)
            this.score++;
        else
            this.score = 0;
    }

}


